$(function () {
    var Accordion = function (el, multiple) {
      this.el = el || {};
      this.multiple = multiple || false;
  
      
      var links = this.el.find(".link");
     
      links.on("click", { el: this.el, multiple: this.multiple }, this.dropdown);
    };
  
    Accordion.prototype.dropdown = function (e) {
      var $el = e.data.el;
      ($this = $(this)), ($next = $this.next());
  
      $next.slideToggle();
      $this.parent().toggleClass("open");
  
      if (!e.data.multiple) {
        $el.find(".submenu").not($next).slideUp().parent().removeClass("open");
      }
    };
  
    var accordion = new Accordion($("#accordion"), false);
  });
  $('.switch-btn').click(function(){
    $(this).toggleClass('switch-on');
    if ($(this).hasClass('switch-on')) {
      $(this).trigger('on.switch');
    } else {
      $(this).trigger('off.switch');
    }
  });
  jQuery(document).ready(function(){
    function htmSliderNotloop(){
      
      
      var viewSliderNum = 3,
        animDuration = 1000,
        slideWrap = jQuery('.slide-wrap'),
        nextLink = jQuery('.next-slide'),
        prevLink = jQuery('.prev-slide'),
        slideNum = slideWrap.find('.slide-item').size(),
        slideWidth = jQuery('.slide-item').outerWidth(),			
        sliderEndPoint = (slideNum - viewSliderNum)*slideWidth*(-1);
  
      
      nextLink.click(function(){
        if(jQuery(this).hasClass('disabled')){
          return false
        } else {
          if(!slideWrap.is(':animated')) {
          
            var leftSlidePos = slideWrap.position().left,
              lsp = Math.round(leftSlidePos/100)*100,
              newLeftPos = lsp - slideWidth,
              nlp = Math.round(newLeftPos/100)*100;
  
            console.log('было: '+lsp+' стало: '+nlp)
              
            slideWrap.animate({left: nlp}, animDuration, function(){
              
              slideWrap.css({'left':nlp});
              
              if(nlp == sliderEndPoint){
                nextLink.addClass('disabled');
              }
              if(nlp != sliderEndPoint) {
                nextLink.removeClass('disabled');
              }
              if(nlp == 0){
                prevLink.addClass('disabled');
              }
              if(nlp != 0){
                prevLink.removeClass('disabled');
              }
              
            });
            
          }
        }
        return false
      });
      
      
      prevLink.click(function(){
      
        if(jQuery(this).hasClass('disabled')){
          return false
        } else {
          if(!slideWrap.is(':animated')) {
  
            var leftSlidePos = slideWrap.position().left,
              lsp = Math.round(leftSlidePos/100)*100,
              newLeftPos = lsp + slideWidth,
              nlp = Math.round(newLeftPos/100)*100;
              
            slideWrap.animate({left: nlp}, animDuration, function(){
            
              slideWrap.css({'left':nlp});
              
              if(nlp == sliderEndPoint){
                nextLink.addClass('disabled');
              }
              if(nlp != sliderEndPoint) {
                nextLink.removeClass('disabled');
              }
              if(nlp == 0){
                prevLink.addClass('disabled');
              }
              if(nlp != 0){
                prevLink.removeClass('disabled');
              }
              
            });
            
          }
        }
        return false		
      });
      
    }
  
    
    htmSliderNotloop();
  });

  window.onscroll = function() {myFunction()};